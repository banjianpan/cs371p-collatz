// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream

#include "Collatz.hpp"

using namespace std;

int cache[1000000] = {0};

//this function performs regular collatz when the number is not in lazy cache
int regular_collatz(int input) {
    int result = 1;
    long input_copy = (long) input;

    while(input_copy != 1) {
        if(input_copy % 2 == 0) {
            input_copy = input_copy >> 1;
            result += 1;
        } else {
            input_copy = (3 * input_copy + 1) >> 1;
            result += 2;
        }
    }
    cache[input] = result;
    assert(input_copy == 1);
    return result;
}

// ------------
// collatz_read
// ------------

istream& collatz_read (istream& r, int& i, int& j) {
    return r >> i >> j;
}

// ------------
// collatz_eval
// ------------

int collatz_eval (int start, int end) {
    // <your code>

    int start_copy = start;
    int end_copy = end;
    int result = 0;

    if(start > end) { // this makes sure that the start is less than the end
        start_copy = end;
        end_copy = start;
    }
    assert(start_copy <= end_copy);
    if(start_copy == 1) {
        ++start_copy;
        result = 1;
    }

    while(start_copy < end_copy + 1) {

        if(cache[start_copy] != 0) {
            if(result < cache[start_copy]) {
                result = cache[start_copy];
            }
        } else {
            int reg_collatz = regular_collatz(start_copy);
            if(reg_collatz > result) {
                result = reg_collatz;
            }
        }
        ++start_copy;
    }
    assert(result > 0);
    return result;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    int i;
    int j;

    while (collatz_read(r, i, j)) {
        const int v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}